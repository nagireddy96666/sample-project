from django.urls import path
from django.contrib import admin
from rest_framework.urlpatterns import format_suffix_patterns
from . import views

urlpatterns = [
    #path('setcookie', views.setcookie.as_view()),
    #path('getcookie', showcookie),
    #path('redirect/', data_flair),
   # path('dataflair/', index),
    path('polls/', views.API_objects.as_view()),
    path('polls/<int:pk>/', views.API_objects_details.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
