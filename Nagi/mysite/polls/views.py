from django.shortcuts import render
from rest_framework import generics
from polls.models import Question
from polls.serializers import POLLS_Serializer

class API_objects(generics.ListCreateAPIView):
    queryset = Question.objects.all()
    serializer_class = POLLS_Serializer

class API_objects_details(generics.RetrieveUpdateDestroyAPIView):
    queryset = Question.objects.all()
    serializer_class = POLLS_Serializer
